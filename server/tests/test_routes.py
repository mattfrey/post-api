from flask import Flask
from server.routes.routes import Routes

# Basic Ping Test

def test_ping_route():
    app = Flask(__name__)
    Routes(app)

    client = app.test_client()
    url = 'api/ping'

    response = client.get(url)
    assert 'success' in response.json.keys() and response.json['success'] == True
    assert response.status_code == 200


# Successful Outputs: Posts Route

def test_posts_no_duplicates():
    app = Flask(__name__)
    Routes(app)

    client = app.test_client()
    url = 'api/posts?tags=science'
    response = client.get(url)

    scanned = set()
    duplicates = []
    if 'posts' in response.json.keys() and len(response.json['posts']) > 0:
        for post in response.json['posts']:
            
            if post['id'] not in scanned:
                scanned.add(post['id'])
            else:
                duplicates.append(post['id'])

    assert len(duplicates) == 0
    assert response.status_code == 200

def test_posts_matching_tag():
    app = Flask(__name__)
    Routes(app)

    client = app.test_client()
    url = 'api/posts?tags=science'

    response = client.get(url)
    assert 'posts' in response.json.keys() and len(response.json['posts']) > 0
    assert 'science' in response.json['posts'][0]['tags']
    assert response.status_code == 200

def test_posts_2_matching_tag():
    app = Flask(__name__)
    Routes(app)

    client = app.test_client()
    url = 'api/posts?tags=science,tech'

    response = client.get(url)
    assert 'posts' in response.json.keys() and len(response.json['posts']) > 0
    assert 'science' in response.json['posts'][0]['tags'] or 'tech' in response.json['posts'][0]['tags']
    assert response.status_code == 200

def test_posts_sortBy_ascending():
    app = Flask(__name__)
    Routes(app)

    client = app.test_client()
    url = 'api/posts?tags=science,tech&sortBy=popularity'

    response = client.get(url)
    assert 'posts' in response.json.keys() and len(response.json['posts']) > 0
    assert response.json['posts'][0]['popularity'] <= response.json['posts'][1]['popularity']
    assert response.status_code == 200

def test_posts_sortBy_descending():
    app = Flask(__name__)
    Routes(app)

    client = app.test_client()
    url = 'api/posts?tags=science,tech&sortBy=likes&direction=desc'

    response = client.get(url)
    assert 'posts' in response.json.keys() and len(response.json['posts']) > 0
    assert response.json['posts'][0]['likes'] > response.json['posts'][1]['likes']
    assert response.status_code == 200

def test_posts_no_matching_tags():
    app = Flask(__name__)
    Routes(app)

    client = app.test_client()
    url = 'api/posts?tags=candy'

    response = client.get(url)
    assert 'posts' in response.json.keys() and len(response.json['posts']) == 0
    assert response.status_code == 200

# Successful Error Messages

def test_posts_no_tags():
    app = Flask(__name__)
    Routes(app)

    client = app.test_client()
    url = 'api/posts'

    response = client.get(url)
    assert 'error' in response.json.keys() and response.json['error'] == 'Tags parameter is required'
    assert response.status_code == 400

def test_posts_wrong_direction():
    app = Flask(__name__)
    Routes(app)

    client = app.test_client()
    url = 'api/posts?tags=science&sortBy=title'

    response = client.get(url)
    assert 'error' in response.json.keys() and response.json['error'] == 'sortBy parameter is invalid.'
    assert response.status_code == 400

def test_posts_wrong_sort_by():
    app = Flask(__name__)
    Routes(app)

    client = app.test_client()
    url = 'api/posts?tags=science&sortBy=id&direction=down'

    response = client.get(url)
    assert 'error' in response.json.keys() and response.json['error'] == 'direction parameter is invalid.'
    assert response.status_code == 400