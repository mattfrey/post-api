from flask import Flask, make_response, request
import requests


def Routes(app):

    @app.route('/api/ping', methods=['GET'])
    def ping():
        return make_response({"success": True }, 200)

    @app.route('/api/posts', methods=['GET'])
    def posts():
        tags = request.args.get('tags') if request.args.get('tags') else False
        sort_by = request.args.get('sortBy') if request.args.get('sortBy') else 'id' 
        direction = request.args.get('direction') if request.args.get('direction') else 'asc' 
        response = {}

        if not tags:
            response['error'] = "Tags parameter is required"
        else:
            if not sort_by in ['id', 'reads', 'likes', 'popularity']:
                response['error'] = "sortBy parameter is invalid."
            if not direction in ['asc','desc']:
                response['error'] = "direction parameter is invalid."

            tags = tags.split(',') if "," in tags else [tags]

            if not "error" in response.keys():
                all_posts = {}

                for t in tags:
                    result = requests.get(f'https://hatchways.io/api/assessment/blog/posts?tag={t}')
                    if len(result.json()['posts']) > 0:
                        for p in result.json()['posts']:
                            all_posts[p['id']] = p

                all_posts = list(all_posts.values())
                all_posts.sort(key = lambda post:post[sort_by])
                if direction == "desc":
                    all_posts.reverse()

                response['posts'] = all_posts

        status_code = 200 if not "error" in  response.keys() else 400
        return make_response(response, status_code)