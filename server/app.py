from flask import Flask, make_response, request
from routes.routes import Routes

app = Flask(__name__)

Routes(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=False)